cmake_minimum_required(VERSION 3.7)

project (CMakeCiTest)

if (MSVC)
	set(CMAKE_CONFIGURATION_TYPES "Debug;Release")
	add_definitions(-DUNICODE -D_UNICODE)
endif()

add_subdirectory(src)
